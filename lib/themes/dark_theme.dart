import 'package:flutter/material.dart';

class DarkTheme {
  static const Color primary = Colors.pink;

  static final ThemeData themeData = ThemeData.dark().copyWith(

      // Color primario
      primaryColor: Colors.indigo,

      // AppBar Theme
      appBarTheme: const AppBarTheme(color: primary, elevation: 0),
      scaffoldBackgroundColor: Colors.black
  );
}
