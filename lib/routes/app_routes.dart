import 'package:flutter/material.dart';
import 'package:flutter_component_examples/models/models.dart';
import 'package:flutter_component_examples/screens/screens.dart';

class AppRoutes {
  static const initialRoute = 'home';

  // TODO: This variable is out of the context, but this is and simple example, should be fix it
  static List<HomeMenuOption> homeMenuOptions = <HomeMenuOption>[
    HomeMenuOption(
        name: 'listview1',
        description: 'Lista tipo 1',
        screen: const ListView1Screen(),
        icon: Icons.list_alt),
    HomeMenuOption(
        name: 'listview2',
        description: 'Lista tipo 2',
        screen: const ListView2Screen(),
        icon: Icons.list_alt),
    HomeMenuOption(
        name: 'messageCard',
        description: 'Card Message',
        screen: const CardsScreen(),
        icon: Icons.add_card),
    HomeMenuOption(
        name: 'alertScreen',
        description: 'Alerta',
        screen: const AlertScreen(),
        icon: Icons.add_alert),
    HomeMenuOption(
        name: 'avatarScreen',
        description: 'Avatar',
        screen: const AvatarScreen(),
        icon: Icons.man),
    HomeMenuOption(
        name: 'animatedScreen',
        description: 'Animated Container',
        screen: const AnimatedScreen(),
        icon: Icons.play_circle_outline),
    HomeMenuOption(
        name: 'formScreen',
        description: 'Forms',
        screen: const FormScreen(),
        icon: Icons.input),
    HomeMenuOption(
        name: 'slidersScreen',
        description: 'Sliders and Checks',
        screen: const SlidersScreen(),
        icon: Icons.check_box_outlined),
    HomeMenuOption(
        name: 'scrollViewBuilder',
        description: 'Scroll view builder',
        screen: const ScrollViewBuilder(),
        icon: Icons.stacked_line_chart),
  ];

  static Map<String, Widget Function(BuildContext)> getGeneratedRoutes() {
    Map<String, Widget Function(BuildContext)> routes = {};

    routes.addAll({'home': (BuildContext context) => const HomeScreen()});

    for (final option in homeMenuOptions) {
      routes.addAll({option.name: (BuildContext context) => option.screen});
    }

    return routes;
  }

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    return MaterialPageRoute(builder: (context) => WelcomeScreen(context));
  }
}
