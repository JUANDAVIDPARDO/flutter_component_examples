import 'package:flutter/cupertino.dart';

class HomeMenuOption {
  final String name;
  final String description;
  final Widget screen;
  final IconData icon;

  HomeMenuOption({
      required this.name,
      required this.description,
      required this.screen,
      required this.icon
  });
}
