import 'package:flutter/material.dart';

class MessageCard extends StatelessWidget {
  const MessageCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Card(
      child: Column(
        children: [
          ListTile(
            title: const Text('Get started using Jira in Teams'),
            subtitle: const Text('''
Here are some of the things you can do: Set up personal notifications to stay on top of your work in Jira View '''),
            leading: const Icon(Icons.access_alarm),
            iconColor: Theme.of(context).iconTheme.color,
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                  onPressed: () { },
                  child: const Text('Sing in'),
                ),
                TextButton(
                  onPressed: () { },
                  child: const Text('take a tour'),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
