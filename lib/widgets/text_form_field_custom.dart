import 'package:flutter/material.dart';

class TextFormFieldCustom extends StatelessWidget {
  final String? hintText;
  final String? labelText;
  final IconData? icon;
  final bool? isCapitalize;
  final TextInputType? textInputType;
  final bool isObscureText;

  final String propertyFormName;
  final Map<String, String> formValues;

  const TextFormFieldCustom(
      {Key? key,
      this.hintText,
      this.labelText,
      this.icon,
      this.isCapitalize,
      this.textInputType,
      this.isObscureText = false,
      required this.propertyFormName,
      required this.formValues})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textCapitalization: isCapitalize != null
          ? TextCapitalization.words
          : TextCapitalization.none,
      keyboardType: textInputType,
      obscureText: isObscureText,
      onChanged: (value) {
        formValues[propertyFormName] = value;
      },
      validator: (value) {
        if (value == '') {
          return 'hola mundo';
        }
      },
      decoration: InputDecoration(
          hintText: hintText,
          labelText: labelText,
          icon: icon != null ? Icon(icon) : null),
    );
  }
}
