import 'package:flutter/material.dart';
import 'package:flutter_component_examples/widgets/text_form_field_custom.dart';

class FormScreen extends StatelessWidget {
  const FormScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> keyForm = GlobalKey<FormState>();
    final Map<String, String> formValues = {
      'first_name': '',
      'last_name': '',
      'email': '',
      'password': '',
      'role': ''
    };

    return Scaffold(
      appBar: AppBar(
        title: const Text('Inputs and Validations'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
          child: Form(
            key: keyForm,
            child: Column(
              children: [
                TextFormFieldCustom(
                  hintText: 'Pepito',
                  labelText: 'Nombre',
                  isCapitalize: true,
                  propertyFormName: 'first_name',
                  formValues: formValues,
                ),
                const SizedBox(height: 30),
                TextFormFieldCustom(
                  hintText: 'Perez',
                  labelText: 'Apellido',
                  isCapitalize: true,
                  propertyFormName: 'last_name',
                  formValues: formValues,
                ),
                const SizedBox(height: 30),
                TextFormFieldCustom(
                  hintText: 'pepito.perez@email.com',
                  labelText: 'e-mail',
                  textInputType: TextInputType.emailAddress,
                  propertyFormName: 'email',
                  formValues: formValues,
                ),
                const SizedBox(height: 30),
                TextFormFieldCustom(
                  labelText: 'Password',
                  isObscureText: true,
                  propertyFormName: 'password',
                  formValues: formValues,
                ),
                const SizedBox(height: 30),
                DropdownButtonFormField<String>(
                  items: const [
                    DropdownMenuItem(value: 'Admin', child: Text('Admin')),
                    DropdownMenuItem(value: 'Teacher', child: Text('teacher')),
                    DropdownMenuItem(value: 'Student', child: Text('Student')),
                    DropdownMenuItem(value: 'Father', child: Text('Father')),
                  ],
                  onChanged: (value) {
                    formValues['role'] = value ?? '';
                  },
                ),
                const SizedBox(height: 30),
                SizedBox(
                  height: 50,
                  width: double.infinity,
                  child: ElevatedButton(
                      onPressed: () {
                        if (keyForm.currentState!.validate()) {
                          print(formValues);
                        }
                        // TODO: Review on focus node because it damage the form
                        // FocusScope.of(context).requestFocus(FocusNode());
                      },
                      child: const Center(
                          child: Text(
                        'Enviar',
                        style: TextStyle(fontSize: 16),
                      ))),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
