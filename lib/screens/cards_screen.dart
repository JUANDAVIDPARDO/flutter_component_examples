import 'package:flutter/material.dart';
import 'package:flutter_component_examples/widgets/widgets.dart';

class CardsScreen extends StatelessWidget {
  final games = const [
    "Assasins Creed",
    "Darksiders",
    "Fifa 2022",
    "Halo",
    "Bioschock"
  ];

  static const int MAX_ITEMS = 10;

  const CardsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Listview 2 Screen"),
        ),
        body: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          children: const [
            MessageCard(),
            SizedBox(height: 10),
            ImageCard(
              imageUrl:
                  'https://i.pinimg.com/originals/a3/40/18/a34018ccbebfc1f5bf6364316001848d.jpg',
              imageDescription: 'reserve wild life',
            ),
            SizedBox(height: 10),
            ImageCard(
              imageUrl: 'https://wallpaperset.com/w/full/3/d/d/22922.jpg',
              imageDescription: 'snowing',
            ),
            SizedBox(height: 10),
            ImageCard(
              imageUrl: 'https://images5.alphacoders.com/823/823751.jpg',
            ),
            SizedBox(height: 10),
            ImageCard(
              imageUrl:
                  'https://wallpapershome.com/images/pages/pic_h/3441.jpg',
              imageDescription: 'eagle flying',
            ),
          ],
        ));
  }

  int _getMaxItemInListView() {
    if (games.length < MAX_ITEMS) {
      return games.length;
    }

    return MAX_ITEMS;
  }
}
