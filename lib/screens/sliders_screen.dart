import 'package:flutter/material.dart';
import 'package:flutter_component_examples/themes/themes.dart';

class SlidersScreen extends StatefulWidget {
  const SlidersScreen({Key? key}) : super(key: key);

  @override
  State<SlidersScreen> createState() => _SlidersScreenState();
}

class _SlidersScreenState extends State<SlidersScreen> {
  double _sliderValue = 100;
  bool _checkboxValue = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sliders and Checks'),
      ),
      body: Column(
        children: [
          Slider.adaptive(
              value: _sliderValue,
              min: 0,
              max: 400,
              activeColor: LightTheme.primary,
              onChanged: _checkboxValue
                  ? (value) {
                      _sliderValue = value;
                      setState(() {});
                    }
                  : null),
          CheckboxListTile(
              title: const Text('Habilitar slider'),
              value: _checkboxValue,
              activeColor: LightTheme.primary,
              onChanged: (value) {
                _checkboxValue = value ?? true;
                setState(() {});
              }),
          SwitchListTile.adaptive(
              title: const Text('Habilitar slider'),
              activeColor: LightTheme.primary,
              value: _checkboxValue,
              onChanged: (value) {
                _checkboxValue = value ?? true;
                setState(() {});
              }),
          const AboutListTile(),
          Expanded(
            child: SingleChildScrollView(
              child: Image(
                image: const NetworkImage(
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSwicTbwVqWF-NH3WREo_LCFvND0jB2xG4uYw&usqp=CAU'),
                width: _sliderValue,
              ),
            ),
          )
        ],
      ),
    );
  }
}
