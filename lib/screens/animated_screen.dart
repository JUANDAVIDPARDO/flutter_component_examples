import 'dart:math';

import 'package:flutter/material.dart';

class AnimatedScreen extends StatefulWidget {
  const AnimatedScreen({Key? key}) : super(key: key);

  @override
  State<AnimatedScreen> createState() => _AnimatedScreenState();
}

class _AnimatedScreenState extends State<AnimatedScreen> {
  Color _color = Colors.amber;
  double _height = 100;
  double _radius = 10;
  double _width = 100;

  void changeShape() {
    // 10 is added to assure that for is visible
    var rng = Random();
    _color =
        Color.fromRGBO(rng.nextInt(255), rng.nextInt(255), rng.nextInt(255), 1);
    _height = rng.nextInt(200).toDouble() + 10;
    _radius = rng.nextInt(50).toDouble();
    _width = rng.nextInt(200).toDouble() + 10;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Animated Container'),
      ),
      body: Center(
        child: AnimatedContainer(
          duration: const Duration(milliseconds: 400),
          curve: Curves.easeInBack,
          height: _height,
          width: _width,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(_radius), color: _color),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: changeShape,
        child: const Icon(
          Icons.play_arrow,
          size: 30,
        ),
      ),
    );
  }
}
