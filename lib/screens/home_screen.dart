import 'package:flutter/material.dart';
import 'package:flutter_component_examples/routes/app_routes.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Example components Flutter'),
      ),
      body: ListView.separated(
          itemBuilder: (BuildContext context, index) => ListTile(
                leading: Icon(AppRoutes.homeMenuOptions[index].icon),
                title: Text(AppRoutes.homeMenuOptions[index].description),
                onTap: () {
                  Navigator.pushNamed(context, AppRoutes.homeMenuOptions[index].name);
                },
              ),
          separatorBuilder: (_, __) => const Divider(),
          itemCount: AppRoutes.homeMenuOptions.length),
    );
  }
}
