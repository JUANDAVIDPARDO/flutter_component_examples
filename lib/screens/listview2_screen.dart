import 'package:flutter/material.dart';

class ListView2Screen extends StatelessWidget {
  final games = const [
    "Assasins Creed",
    "Darksiders",
    "Fifa 2022",
    "Halo",
    "Bioschock"
  ];

  static const int MAX_ITEMS = 10;

  const ListView2Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Listview 2 Screen"),
        ),
        body: ListView.separated(
          itemCount: _getMaxItemInListView(),
          itemBuilder: (context, index) => ListTile(
            title: Text(games[index]),
            trailing: const Icon(Icons.gamepad_outlined),
            onTap: () => print(games[index]),
          ),
          separatorBuilder: (_, __) => const Divider(),
        ));
  }

  int _getMaxItemInListView() {
    if (games.length < MAX_ITEMS) {
      return games.length;
    }

    return MAX_ITEMS;
  }
}
