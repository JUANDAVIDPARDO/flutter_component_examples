import 'package:flutter/material.dart';

class ListView1Screen extends StatelessWidget {

  final games = const[
    "Assasins Creed",
    "Darksiders",
    "Fifa 2022",
    "Halo",
    "Bioschock"
  ];

  const ListView1Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Listview 1 Screen"),
      ),
      body: ListView(
        children: games.map((game) => ListTile(
            title: Text(game),
            leading: const Icon(Icons.gamepad_outlined),
          )).toList(),
      ),
    );
  }
}
