import 'package:flutter/material.dart';
import 'package:flutter_component_examples/themes/light_theme.dart';

class ScrollViewBuilder extends StatefulWidget {
  const ScrollViewBuilder({Key? key}) : super(key: key);

  @override
  State<ScrollViewBuilder> createState() => _ScrollViewBuilderState();
}

class _ScrollViewBuilderState extends State<ScrollViewBuilder> {
  List<int> imagesIds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  bool isLoading = false;
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    scrollController.addListener(() {
      if ((scrollController.position.maxScrollExtent + 200) <=
          scrollController.position.pixels) {
        fetchData();
      }
    });
  }

  void add5() {
    int lastId = imagesIds.last;
    imagesIds.addAll([1, 2, 3, 4, 5].map((e) => lastId + e));
    setState(() {});
  }

  Future fetchData() async {
    if (isLoading) return;

    isLoading = true;
    setState(() {});

    await Future.delayed(const Duration(seconds: 3));

    add5();
    isLoading = false;
    setState(() {});

    if ((scrollController.position.pixels + 100) >=
        scrollController.position.maxScrollExtent) {
      scrollController.position.animateTo(
        (scrollController.position.pixels + 100),
        duration: const Duration(milliseconds: 400),
        curve: Curves.easeInExpo,
      );
    }
  }

  Future<void> listImagesRefreshIndicator () async {
    await Future.delayed(const Duration(seconds: 2));
    final firstIdInLastSet = imagesIds.last;
    imagesIds.clear();
    print(firstIdInLastSet);
    imagesIds.add(firstIdInLastSet + 1);
    add5();
    print(imagesIds);
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: MediaQuery.removePadding(
        context: context,
        removeTop: true,
        removeBottom: true,
        child: Stack(
          children: [
            RefreshIndicator(
              color: LightTheme.primary,
              onRefresh: listImagesRefreshIndicator,
              child: ListView.builder(
                controller: scrollController,
                itemCount: imagesIds.length,
                itemBuilder: (BuildContext context, int index) {
                  return FadeInImage(
                    width: double.infinity,
                    height: 300,
                    fit: BoxFit.cover,
                    image: NetworkImage(
                        'https://picsum.photos/200/300?image=${imagesIds[index]}'),
                    placeholder:
                        const AssetImage('lib/assets/images/loading.gif'),
                    fadeOutDuration: const Duration(milliseconds: 200),
                    fadeInDuration: const Duration(milliseconds: 100),
                  );
                },
                physics: const BouncingScrollPhysics(),
              ),
            ),
            if (isLoading)
              Positioned(
                  bottom: 50,
                  left: size.width * .5 - 30,
                  child: const _LoadingIcon())
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(
          Icons.close,
          size: 16,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}

class _LoadingIcon extends StatelessWidget {
  const _LoadingIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.7),
        shape: BoxShape.circle,
      ),
      height: 60,
      padding: const EdgeInsets.all(10),
      width: 60,
      child: const CircularProgressIndicator(
        color: LightTheme.primary,
      ),
    );
  }
}
